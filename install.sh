#!/usr/bin/env bash
path=`pwd`

apt-get update
apt-get install nginx -y

# install redis
cd ~
wget http://download.redis.io/releases/redis-4.0.11.tar.gz
tar -xzf redis-4.0.11.tar.gz
cd redis-4.0.11
make

# start redis with background
src/redis-server --daemonize yes


# install pyenv
cd ~
curl -L https://github.com/pyenv/pyenv-installer/raw/master/bin/pyenv-installer | bash
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bash_profile
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bash_profile
echo -e 'if command -v pyenv 1>/dev/null 2>&1; then\n  eval "$(pyenv init -)"\nfi' >> ~/.bash_profile
source ~/.bash_profile

# install python lib
eval "$(pyenv init -)"
pyenv versions
pyenv virtualenv myblog
pyenv activate myblog
cd $path
pip install -r ./requirements.txt

# link my nginx.conf
ln -s $path/myblog_nginx.conf /etc/nginx/sites-enabled/

# start nginx
mkdir /var/www/ssl
cp ./server.key /var/www/ssl
cp ./server.crt /var/www/ssl
service nginx start

# start project
python manage.py collectstatic
python manage.py makemigrations
python manage.py migrate
#uwsgi uwsgi.ini

# install supervisor
pyenv install 2.7.15
pyenv virtualenv 2.7.15 supervisor
pyenv activate supervisor
pip install supervisor
supervisord -c supervisord.conf
supervisorctl status