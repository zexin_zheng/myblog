Django 个人博客项目

[![pipeline status](https://gitlab.com/zexin_zheng/myblog/badges/master/pipeline.svg)](https://gitlab.com/zexin_zheng/myblog/commits/master)
[![coverage report](https://gitlab.com/zexin_zheng/myblog/badges/master/coverage.svg)](https://gitlab.com/zexin_zheng/myblog/commits/master)

## 安装依赖

```shell
pip install -r requirements.txt
```

## 迁移同步数据库

```shell
python manage.py makemigrations
python manage.py migrate
```

## 启动项目

```shell
python manage.py runserver 
```

