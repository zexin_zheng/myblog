import re

from django.test import TestCase
from django_redis import get_redis_connection
from django.core.cache import cache
import markdown

from .models import Author, Post, Tag, Category


# Create your tests here.

class AuthorTest(TestCase):
    def test_create(self):
        Author(name='alice', age=18).save()
        author = Author.objects.get(name='alice', age=18)
        self.assertEqual(author.name, 'alice')


class RedisCacheTest(TestCase):
    def setUp(self):
        get_redis_connection('default').flushall()

    def tearDown(self):
        get_redis_connection("default").flushall()

    def test_cache(self):
        cache.set('name', 'alice', nx=True)
        self.assertEqual(cache.get('name'), 'alice')


class PostTest(TestCase):
    def setUp(self):
        self.author = Author(name='alice', age=18)
        self.author.save()
        self.tag = Tag(name='django')
        self.tag.save()
        self.category = Category(name='python')
        self.category.save()

    def test_save(self):
        content = """
        hello world
        <!--more-->
        ```python
        def hello():
            print('hello world')
        ```
        """
        post = Post(title='test', content=content, author=self.author, category=self.category)
        post.save()
        post.tags.add(self.tag)

        query_post = Post.objects.get(id=post.id)
        self.assertEqual(query_post.title, 'test')
        self.assertEqual(query_post.tags.all().first().name, 'django')
        self.assertEqual(query_post.content, content)
        self.assertEqual(query_post.category.name, 'python')
        md = markdown.Markdown(extensions=['markdown.extensions.extra', 'markdown.extensions.codehilite',
                                           'markdown.extensions.toc'])
        self.assertEqual(query_post.html, md.convert(content))
        self.assertEqual(query_post.toc, md.toc)
        regex = re.compile('(.*)<!--more-->.*', re.DOTALL)
        result = regex.match(content)
        self.assertEqual(query_post.excerpt, md.convert(result.group(1)))

        new_content = """
              hello world
              ```python
              def hello():
                  print('hello world')
              ```
              """
        query_post.content = new_content
        query_post.save()

        query_post2 = Post.objects.get(id=post.id)
        self.assertEqual(query_post2.excerpt, '')
        self.assertEqual(query_post2.content, new_content)
