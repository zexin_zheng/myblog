from django.shortcuts import render
from django.views.generic import DetailView, ListView

from .models import Post


# Create your views here.

class PostDetailView(DetailView):
    template_name = 'detail.html'
    model = Post


class PostListView(ListView):
    template_name = 'index.html'
    model = Post
    queryset = Post.objects.all()
