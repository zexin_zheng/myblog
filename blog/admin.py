from django.contrib import admin
from django.utils.html import format_html
from django.shortcuts import reverse

from .models import Author, Post, Comment, Tag, Category


# Register your models here.

@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    fields = ('name', 'email', 'age', 'birthday',)
    list_display = ('name', 'email',)
    search_fields = ('name', 'email',)


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    class Media:
        css = {
            "all": ("css/highlight/emacs.css",)
        }

    fields = (
        'title', 'excerpt', 'content', 'author', 'create_time', 'last_update_time', 'show_html', 'toc', 'tags',
        'category',)
    readonly_fields = ('create_time', 'last_update_time', 'show_html', 'toc', 'excerpt',)
    list_display = ('title', 'author', 'create_time', 'last_update_time')
    search_fields = ('title', 'content', 'author__name',)
    autocomplete_fields = ('author', 'tags', 'category',)

    def show_html(self, obj):
        return format_html(obj.html)

    show_html.short_description = '正文预览'


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    fields = ('name', 'email', 'content', 'post',)
    list_display = ('post', 'name', 'email',)
    search_fields = ('post__title', 'name', 'email',)


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    fields = ('name',)
    list_display = ('name',)
    search_fields = ('name',)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    fields = ('name',)
    list_display = ('name',)
    search_fields = ('name',)


admin.AdminSite.site_header = 'MyBlog 后台管理'
admin.AdminSite.site_url = reverse('blog:list')
