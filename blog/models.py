import re

import markdown
from django.db import models


# Create your models here.


class Post(models.Model):
    """
    文章
    """
    title = models.CharField(max_length=100, default='', verbose_name='题目')
    content = models.TextField(verbose_name='正文')
    html = models.TextField(verbose_name='正文html', blank=True, null=True)
    toc = models.TextField(verbose_name='目录', blank=True, null=True)
    author = models.ForeignKey('Author', on_delete=models.CASCADE, verbose_name='作者')
    tags = models.ManyToManyField('Tag', verbose_name='标签', blank=True)
    category = models.ForeignKey('Category', verbose_name='分类', on_delete=models.CASCADE, blank=True, null=True)
    excerpt = models.TextField(verbose_name='摘要', blank=True, null=True)

    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间')
    last_update_time = models.DateTimeField(auto_now=True, verbose_name='最后更新时间')

    def save(self, *args, **kwargs):
        md = markdown.Markdown(extensions=['markdown.extensions.extra', 'markdown.extensions.codehilite',
                                           'markdown.extensions.toc'])
        regex = re.compile('(.*)<!--more-->', re.DOTALL)
        result = regex.match(self.content)
        if result:
            self.excerpt = md.convert(result.group(1))
        else:
            self.excerpt = ''
        self.html = md.convert(self.content)
        self.toc = md.toc
        super().save(*args, **kwargs)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('-last_update_time',)


class Author(models.Model):
    """
    作者
    """
    name = models.CharField(max_length=100, default='', verbose_name='名字')
    email = models.EmailField(verbose_name='邮箱')
    age = models.IntegerField(verbose_name='年龄', null=True, blank=True)
    birthday = models.DateField(verbose_name='生日', null=True, blank=True)

    def __str__(self):
        return self.name


class Comment(models.Model):
    """
    评论
    """
    name = models.CharField(max_length=100, verbose_name='名字')
    email = models.EmailField(verbose_name='邮箱')
    content = models.TextField(verbose_name='内容')
    post = models.ForeignKey('Post', on_delete=models.CASCADE, verbose_name='文章')

    def __str__(self):
        return self.name


class Tag(models.Model):
    """
    标签
    """
    name = models.CharField(max_length=100, verbose_name='标签')

    def __str__(self):
        return self.name


class Category(models.Model):
    """
    分类
    """
    name = models.CharField(max_length=100, verbose_name='分类')

    def __str__(self):
        return self.name
